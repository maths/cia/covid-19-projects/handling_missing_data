#! /usr/bin/env python3

import argparse
import logging

from common import (ParamDict, build_parser_experiment, get_experiment_params,
                    process_args_experiment)
from imputation_utilities import impute_and_store
from utilities import init_logger

logger = logging.getLogger("imputation")
init_logger(logger)


def parse_args():
    parser = argparse.ArgumentParser()
    build_parser_experiment(parser)
    parser.add_argument(
        "--loglevel",
        help="Logging level to use; if not specified, default is WARNING",
        choices=["ERROR", "WARNING", "INFO", "DEBUG"],
        default="INFO",
    )

    args = parser.parse_args()
    return process_args(args)


def split_list_arg(arg, convert):
    components = arg.split()
    return list(map(convert, components))


def process_args(args):
    """Process arguments into parameter dictionary"""

    params: ParamDict = {}
    process_args_experiment(args, params)

    logger.setLevel(args.loglevel)

    return params


def perform_imputations(params):
    for experiment in get_experiment_params(params):
        logger.info("Beginning imputation for %s", experiment)
        impute_and_store(experiment, random_state=experiment.m)


def main():
    params = parse_args()
    perform_imputations(params)


if __name__ == "__main__":
    main()
